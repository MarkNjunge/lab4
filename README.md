# LAB 4
## Setup instructions
1. Ensure you have the latest database structure from [here](https://bitbucket.org/MarkNjunge/lab4/downloads/).
2. Clone or download the repository and open it in IntelliJ IDEA (Any IDE will work).
3. Download _jfoenix.jar_ from [here](https://bitbucket.org/MarkNjunge/lab4/downloads/).
4. Mark the folder _src_ as Sources Root (Right click -> Mark Directory as > Sources Root).
5. Go to _Project Settings > Project_ and select Java 1.8+ as the project SDK with a language level of 8.
6. Go to _Project Settings > Libraries_ and add _mysql-connector-java_ and _jfoenix_
7. Go to _Project Settings > Modules/ProjectName/Sources_ and add a new folder called _out_ which should be marked as Excluded.
8. In _Project Settings > Modules/ProjectName/Sources_, mark the directory src/Resources as Resources.
9. Go to Project Settings > Project and add the path to folder made in step 7 under _Project compiler output_.
10. Add a run configuration with [these](http://imgur.com/a/haY8Q) settings.