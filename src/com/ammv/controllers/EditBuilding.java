package com.ammv.controllers;

import com.ammv.database.BuildingHelper;
import com.ammv.database.DBConnector;
import com.ammv.models.Building;
import com.ammv.utils.MenuBarProvider;
import com.ammv.utils.RandomUtils;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class EditBuilding implements Initializable {
    @FXML
    private BorderPane borderPane;
    @FXML
    private JFXComboBox<String> cbBuildingCode;
    @FXML
    private JFXButton btnFind;
    @FXML
    private JFXTextField txtName;
    @FXML
    private JFXButton btnSave;
    @FXML
    private JFXButton btnDelete;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        MenuBarProvider menuBarProvider = new MenuBarProvider(borderPane);
        borderPane.setTop(menuBarProvider.getMenuBar());

        btnFind.setOnAction(event -> findBuilding());
        btnSave.setOnAction(event -> saveBuilding());
        btnDelete.setOnAction(event -> deleteBuilding());

        setBuildingCodes();
    }

    private void setBuildingCodes() {
        List<Building> buildings = BuildingHelper.readAll(reason -> RandomUtils.showAlert(Alert.AlertType.ERROR, reason));

        List<String> buildingCodes = new ArrayList<>();

        for (Building building : buildings) {
            buildingCodes.add(building.getCode());
        }

        cbBuildingCode.getItems().setAll(buildingCodes);
    }

    private void findBuilding() {
        String buildingCode = cbBuildingCode.getValue();

        if (buildingCode == null){
            RandomUtils.showAlert(Alert.AlertType.ERROR, "No building has been selected");
            return;
        }

        Building building = BuildingHelper.readOne(buildingCode, reason -> RandomUtils.showAlert(Alert.AlertType.ERROR, reason));

        txtName.setText(building.getName());

        txtName.setDisable(false);
        btnDelete.setDisable(false);
        btnSave.setDisable(false);
    }

    private void saveBuilding() {
        String buildingCode = cbBuildingCode.getValue();
        String name = txtName.getText().trim();

        if (name.isEmpty()){
            RandomUtils.showAlert(Alert.AlertType.ERROR, "All fields are required");
            return;
        }

        BuildingHelper.update(new Building(buildingCode, name), new DBConnector.DBTask() {
            @Override
            public void taskFailed(String reason) {
                RandomUtils.showAlert(Alert.AlertType.ERROR, reason);
            }

            @Override
            public void taskSuccessful() {
                RandomUtils.showAlert(Alert.AlertType.INFORMATION, "The building has been updated.");
                clearFields();
            }
        });

        setBuildingCodes();
    }

    private void deleteBuilding() {
        BuildingHelper.delete(cbBuildingCode.getValue(), new DBConnector.DBTask() {
            @Override
            public void taskFailed(String reason) {
                RandomUtils.showAlert(Alert.AlertType.ERROR, reason);
            }

            @Override
            public void taskSuccessful() {
                RandomUtils.showAlert(Alert.AlertType.INFORMATION, "The building has been deleted.");
                clearFields();
            }
        });

        setBuildingCodes();
    }

    private void clearFields(){
        cbBuildingCode.setValue("");
        txtName.clear();
    }
}
