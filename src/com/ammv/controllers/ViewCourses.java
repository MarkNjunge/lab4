package com.ammv.controllers;

import com.ammv.database.CourseHelper;
import com.ammv.models.Course;
import com.ammv.utils.MenuBarProvider;
import com.ammv.utils.RandomUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class ViewCourses implements Initializable {
    @FXML
    private BorderPane borderPane;
    @FXML
    private TableView<Course> tvCourses;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        MenuBarProvider menuBarProvider = new MenuBarProvider(borderPane);
        borderPane.setTop(menuBarProvider.getMenuBar());

        TableColumn<Course, String> colCode = new TableColumn<>("Course Code");
        TableColumn<Course, String> colName = new TableColumn<>("Course Name");

        colCode.setCellValueFactory(new PropertyValueFactory<>("code"));
        colName.setCellValueFactory(new PropertyValueFactory<>("name"));

        tvCourses.getColumns().addAll(colCode, colName);

        List<Course> courses = CourseHelper.readAll(reason -> RandomUtils.showAlert(Alert.AlertType.ERROR, reason));
        ObservableList<Course> data = FXCollections.observableList(courses);
        tvCourses.setItems(data);
    }
}
