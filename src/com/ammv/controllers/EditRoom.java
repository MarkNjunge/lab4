package com.ammv.controllers;

import com.ammv.database.BuildingHelper;
import com.ammv.database.DBConnector;
import com.ammv.database.RoomHelper;
import com.ammv.models.Building;
import com.ammv.models.Room;
import com.ammv.utils.MenuBarProvider;
import com.ammv.utils.RandomUtils;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class EditRoom implements Initializable {
    @FXML
    private BorderPane borderPane;
    @FXML
    private JFXComboBox<String> cbRoomCode;
    @FXML
    private JFXButton btnFind;
    @FXML
    private JFXComboBox<String> cbBuildingCode;
    @FXML
    private JFXTextField txtCapacity;
    @FXML
    private JFXComboBox<String> cbRoomType;
    @FXML
    private JFXButton btnSave;
    @FXML
    private JFXButton btnDelete;

    private List<String> buildingCodes;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        MenuBarProvider menuBarProvider = new MenuBarProvider(borderPane);
        borderPane.setTop(menuBarProvider.getMenuBar());

        btnFind.setOnAction(event -> findRoom());
        btnSave.setOnAction(event -> saveRoom());
        btnDelete.setOnAction(event -> deleteRoom());

        setRoomCodes();
        getBuildingCodes();
        cbRoomType.getItems().setAll("Classroom", "Lab");
    }

    private void setRoomCodes() {
        List<Room> rooms = RoomHelper.readAll(reason -> RandomUtils.showAlert(Alert.AlertType.ERROR, reason));

        List<String> roomCodes = new ArrayList<>();

        for (Room room : rooms) {
            roomCodes.add(room.getCode());
        }

        cbRoomCode.getItems().setAll(roomCodes);
    }

    private void getBuildingCodes() {
        List<Building> buildings = BuildingHelper.readAll(reason -> RandomUtils.showAlert(Alert.AlertType.ERROR, reason));

        buildingCodes = new ArrayList<>();

        for (Building building : buildings) {
            buildingCodes.add(building.getCode());
        }
    }

    private void findRoom() {
        String roomCodes = cbRoomCode.getValue();

        if (roomCodes == null) {
            RandomUtils.showAlert(Alert.AlertType.ERROR, "No room has been selected");
            return;
        }

        Room room = RoomHelper.readOne(roomCodes, reason -> RandomUtils.showAlert(Alert.AlertType.ERROR, reason));

        txtCapacity.setText(String.valueOf(room.getCapacity()));

        cbBuildingCode.getItems().setAll(buildingCodes);
        cbBuildingCode.setValue(room.getBuildingCode());

        int lab = room.isLab();
        if (lab == 0) {
            cbRoomType.setValue("Classroom");
        } else {
            cbRoomType.setValue("Lab");
        }

        txtCapacity.setDisable(false);
        cbBuildingCode.setDisable(false);
        cbRoomType.setDisable(false);
        btnDelete.setDisable(false);
        btnSave.setDisable(false);
    }

    private void saveRoom() {
        if (txtCapacity.getText().trim().isEmpty()){
            RandomUtils.showAlert(Alert.AlertType.ERROR, "All fields are required");
            return;
        }

        String roomCode = cbRoomCode.getValue();
        int capacity = Integer.parseInt(txtCapacity.getText().trim());
        String buildingCodes = cbBuildingCode.getValue();
        String roomType = cbRoomType.getValue();

        int roomTypeCode = 0;
        if (roomType.equals("Lab")) {
            roomTypeCode = 1;
        }

        RoomHelper.update(new Room(roomCode, buildingCodes, capacity, roomTypeCode), new DBConnector.DBTask() {
            @Override
            public void taskFailed(String reason) {
                RandomUtils.showAlert(Alert.AlertType.ERROR, reason);
            }

            @Override
            public void taskSuccessful() {
                RandomUtils.showAlert(Alert.AlertType.INFORMATION, "The room has been updated.");
                clearFields();
            }
        });

        clearFields();
        setRoomCodes();
    }

    private void deleteRoom() {
        String roomCodes = cbRoomCode.getValue();

        RoomHelper.delete(roomCodes, new DBConnector.DBTask() {
            @Override
            public void taskFailed(String reason) {
                RandomUtils.showAlert(Alert.AlertType.ERROR, reason);
            }

            @Override
            public void taskSuccessful() {
                RandomUtils.showAlert(Alert.AlertType.INFORMATION, "The room has been deleted");
                clearFields();
            }
        });

        setRoomCodes();
    }

    private void clearFields() {
        cbRoomCode.setValue("");
        cbBuildingCode.setValue("");
        cbRoomType.setValue("");
        txtCapacity.clear();
    }
}
