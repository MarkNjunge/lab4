package com.ammv.controllers;

import com.ammv.database.BuildingHelper;
import com.ammv.models.Building;
import com.ammv.utils.MenuBarProvider;
import com.ammv.utils.RandomUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class ViewBuildings implements Initializable{
    @FXML
    private BorderPane borderPane;
    @FXML
    private TableView<Building> tvBuildings;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        MenuBarProvider menuBarProvider = new MenuBarProvider(borderPane);
        borderPane.setTop(menuBarProvider.getMenuBar());

        TableColumn<Building, String> colCode = new TableColumn<>("Building Code");
        TableColumn<Building, String> colName = new TableColumn<>("Building Name");

        colCode.setCellValueFactory(new PropertyValueFactory<>("code"));
        colName.setCellValueFactory(new PropertyValueFactory<>("name"));

        tvBuildings.getColumns().addAll(colCode, colName);

        List<Building> buildings = BuildingHelper.readAll(reason -> RandomUtils.showAlert(Alert.AlertType.ERROR, reason));
        ObservableList<Building> data = FXCollections.observableList(buildings);
        tvBuildings.setItems(data);
    }
}
