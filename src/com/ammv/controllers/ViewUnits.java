package com.ammv.controllers;

import com.ammv.database.UnitHelper;
import com.ammv.models.Unit;
import com.ammv.utils.MenuBarProvider;
import com.ammv.utils.RandomUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class ViewUnits implements Initializable{
    @FXML
    private BorderPane borderPane;
    @FXML
    private TableView<Unit> tvUnits;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        MenuBarProvider menuBarProvider = new MenuBarProvider(borderPane);
        borderPane.setTop(menuBarProvider.getMenuBar());

        TableColumn<Unit, String> colCode = new TableColumn<>("Unit Code");
        TableColumn<Unit, String> colName = new TableColumn<>("Unit Name");

        colCode.setCellValueFactory(new PropertyValueFactory<>("code"));
        colName.setCellValueFactory(new PropertyValueFactory<>("name"));

        tvUnits.getColumns().addAll(colCode, colName);

        List<Unit> list = UnitHelper.readAll(reason -> RandomUtils.showAlert(Alert.AlertType.ERROR, reason));
        ObservableList<Unit> data = FXCollections.observableList(list);
        tvUnits.setItems(data);
    }
}
