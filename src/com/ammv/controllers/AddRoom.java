package com.ammv.controllers;

import com.ammv.database.BuildingHelper;
import com.ammv.database.DBConnector;
import com.ammv.database.RoomHelper;
import com.ammv.models.Building;
import com.ammv.models.Room;
import com.ammv.utils.MenuBarProvider;
import com.ammv.utils.RandomUtils;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class AddRoom implements Initializable {
    @FXML
    private BorderPane borderPane;
    @FXML
    private JFXComboBox<String> cbBuildingCode;
    @FXML
    private JFXTextField txtCode;
    @FXML
    private JFXTextField txtCapacity;
    @FXML
    private JFXComboBox<String> cbRoomType;
    @FXML
    private JFXButton btnAdd;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        MenuBarProvider menuBarProvider = new MenuBarProvider(borderPane);
        borderPane.setTop(menuBarProvider.getMenuBar());
        btnAdd.setOnAction(event -> addRoom());
        List<Building> buildings = BuildingHelper.readAll(reason -> RandomUtils.showAlert(Alert.AlertType.ERROR, reason));

        List<String> buildingCodes = new ArrayList<>();
        for (Building building : buildings) {
            buildingCodes.add(building.getCode());
        }

        cbBuildingCode.getItems().addAll(buildingCodes);
        cbRoomType.getItems().addAll("Classroom", "Lab");
    }

    private void addRoom() {
        String code = txtCode.getText().trim();
        String capacity = txtCapacity.getText().trim();
        String buildingCode = cbBuildingCode.getValue();
        String roomType = cbRoomType.getValue();

        if (code.isEmpty() || capacity.isEmpty() || buildingCode.isEmpty() || roomType.isEmpty()){
            RandomUtils.showAlert(Alert.AlertType.ERROR, "All fields are required");
            return;
        }

        int roomTypeCode = 0;
        if (roomType.equals("Lab")) {
            roomTypeCode = 1;
        }

        RoomHelper.create(new Room(code, buildingCode, Integer.parseInt(capacity), roomTypeCode), new DBConnector.DBTask() {
            @Override
            public void taskFailed(String reason) {
                RandomUtils.showAlert(Alert.AlertType.ERROR, reason);
            }

            @Override
            public void taskSuccessful() {
                RandomUtils.showAlert(Alert.AlertType.INFORMATION, "The room has been added");
                clearFields();
            }
        });
    }

    private void clearFields() {
        txtCapacity.clear();
        txtCode.clear();
    }
}
