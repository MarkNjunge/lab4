package com.ammv.controllers;

import com.ammv.database.RoomHelper;
import com.ammv.models.Room;
import com.ammv.utils.MenuBarProvider;
import com.ammv.utils.RandomUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class ViewRooms implements Initializable{
    @FXML
    private BorderPane borderPane;
    @FXML
    private TableView<Room> tvRooms;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        MenuBarProvider menuBarProvider = new MenuBarProvider(borderPane);
        borderPane.setTop(menuBarProvider.getMenuBar());

        TableColumn<Room, String> colCode = new TableColumn<>("Room Code");
        TableColumn<Room, String> colBuildCode = new TableColumn<>("Building Code");
        TableColumn<Room, Integer> colCapacity = new TableColumn<>("Capacity");
        TableColumn<Room, Integer> colLab = new TableColumn<>("Lab");

        colCode.setCellValueFactory(new PropertyValueFactory<>("code"));
        colBuildCode.setCellValueFactory(new PropertyValueFactory<>("buildingCode"));
        colCapacity.setCellValueFactory(new PropertyValueFactory<>("capacity"));
        colLab.setCellValueFactory(new PropertyValueFactory<>("lab"));

        tvRooms.getColumns().addAll(colCode, colBuildCode, colCapacity, colLab);

        List<Room> rooms = RoomHelper.readAll(reason -> RandomUtils.showAlert(Alert.AlertType.ERROR, reason));
        ObservableList<Room> data = FXCollections.observableList(rooms);
        tvRooms.setItems(data);
    }
}
