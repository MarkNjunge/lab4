package com.ammv.controllers;

import com.ammv.database.CourseHelper;
import com.ammv.database.DBConnector;
import com.ammv.models.Course;
import com.ammv.utils.MenuBarProvider;
import com.ammv.utils.RandomUtils;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class EditCourse implements Initializable {
    @FXML
    private BorderPane borderPane;
    @FXML
    private JFXComboBox<String> cbCourseCode;
    @FXML
    private JFXButton btnFind;
    @FXML
    private JFXTextField txtName;
    @FXML
    private JFXButton btnSave;
    @FXML
    private JFXButton btnDelete;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        MenuBarProvider menuBarProvider = new MenuBarProvider(borderPane);
        borderPane.setTop(menuBarProvider.getMenuBar());

        setCourseCodes();

        btnFind.setOnAction(event -> findCourse());
        btnSave.setOnAction(event -> saveCourse());
        btnDelete.setOnAction(event -> deleteCourse());
    }

    private void setCourseCodes() {
        List<Course> courses = CourseHelper.readAll(reason -> RandomUtils.showAlert(Alert.AlertType.ERROR, reason));

        List<String> courseCodes = new ArrayList<>();
        for (Course course : courses) {
            courseCodes.add(course.getCode());
        }

        cbCourseCode.getItems().setAll(courseCodes);
    }

    private void findCourse() {
        String courseCode = cbCourseCode.getValue();

        if (courseCode == null) {
            RandomUtils.showAlert(Alert.AlertType.ERROR, "No course has been selected");
            return;
        }

        Course course = CourseHelper.readOne(courseCode, reason -> RandomUtils.showAlert(Alert.AlertType.ERROR, reason));

        txtName.setText(course.getName());

        txtName.setDisable(false);
        btnDelete.setDisable(false);
        btnSave.setDisable(false);
    }

    private void saveCourse() {
        String courseCode = cbCourseCode.getValue();
        String name = txtName.getText().trim();

        if (name.isEmpty()){
            RandomUtils.showAlert(Alert.AlertType.ERROR, "All fields are required");
            return;
        }

        CourseHelper.update(new Course(courseCode, name), new DBConnector.DBTask() {
            @Override
            public void taskFailed(String reason) {
                RandomUtils.showAlert(Alert.AlertType.ERROR, reason);
            }

            @Override
            public void taskSuccessful() {
                RandomUtils.showAlert(Alert.AlertType.INFORMATION, "The course has been updated");
                clearFields();
            }
        });

        setCourseCodes();
    }

    private void deleteCourse() {
        String courseCode = cbCourseCode.getValue();
        CourseHelper.delete(courseCode, new DBConnector.DBTask() {
            @Override
            public void taskFailed(String reason) {
                RandomUtils.showAlert(Alert.AlertType.ERROR, reason);
            }

            @Override
            public void taskSuccessful() {
                RandomUtils.showAlert(Alert.AlertType.INFORMATION, "The course has been deleted.");
                clearFields();
            }
        });

        setCourseCodes();
    }

    private void clearFields(){
        cbCourseCode.setValue("");
        txtName.clear();
    }

}
