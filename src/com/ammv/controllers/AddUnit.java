package com.ammv.controllers;

import com.ammv.database.DBConnector;
import com.ammv.database.UnitHelper;
import com.ammv.models.Unit;
import com.ammv.utils.MenuBarProvider;
import com.ammv.utils.RandomUtils;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.util.ResourceBundle;

public class AddUnit implements Initializable {
    @FXML
    private BorderPane borderPane;
    @FXML
    private JFXTextField txtCode;
    @FXML
    private JFXTextField txtName;
    @FXML
    private JFXButton btnAdd;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        MenuBarProvider menuBarProvider = new MenuBarProvider(borderPane);
        borderPane.setTop(menuBarProvider.getMenuBar());
        btnAdd.setOnAction(event -> addUnit());
    }

    private void addUnit() {
        String name = txtName.getText().trim();
        String code = txtCode.getText().trim();

        if (name.isEmpty() || code.isEmpty()){
            RandomUtils.showAlert(Alert.AlertType.ERROR, "All fields are required!");
            return;
        }

        UnitHelper.create(new Unit(code, name), new DBConnector.DBTask() {
            @Override
            public void taskFailed(String reason) {
                RandomUtils.showAlert(Alert.AlertType.ERROR, reason);
            }

            @Override
            public void taskSuccessful() {
                RandomUtils.showAlert(Alert.AlertType.INFORMATION, "The Unit has been added");
                clearFields();
            }
        });
    }

    private void clearFields() {
        txtName.clear();
        txtCode.clear();
    }
}
