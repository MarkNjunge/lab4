package com.ammv.controllers;

import com.ammv.database.BuildingHelper;
import com.ammv.database.DBConnector;
import com.ammv.models.Building;
import com.ammv.utils.MenuBarProvider;
import com.ammv.utils.RandomUtils;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.util.ResourceBundle;

public class AddBuilding implements Initializable {
    @FXML
    private BorderPane borderPane;
    @FXML
    private JFXTextField txtName;
    @FXML
    private JFXTextField txtCode;
    @FXML
    private JFXButton btnAdd;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // A Node has to be passed instead of a stage because at this point
        // The stage can not be obtained since the window isn't visible
        MenuBarProvider menuBarProvider = new MenuBarProvider(borderPane);
        borderPane.setTop(menuBarProvider.getMenuBar());

        btnAdd.setOnAction(event -> addBuilding());
    }

    private void addBuilding() {
        String name = txtName.getText().trim();
        String code = txtCode.getText().trim();

        if (name.isEmpty() || code.isEmpty()){
            RandomUtils.showAlert(Alert.AlertType.ERROR, "All fields are required!");
            return;
        }

        BuildingHelper.createBuilding(new Building(code, name), new DBConnector.DBTask() {
            @Override
            public void taskFailed(String reason) {
                RandomUtils.showAlert(Alert.AlertType.ERROR, reason);
            }

            @Override
            public void taskSuccessful() {
                RandomUtils.showAlert(Alert.AlertType.INFORMATION, "The building has been added!");
                clearFields();
            }
        });
    }

    private void clearFields(){
        txtCode.clear();
        txtName.clear();
    }
}
