package com.ammv.controllers;

import com.ammv.database.CourseHelper;
import com.ammv.database.DBConnector;
import com.ammv.database.UnitHelper;
import com.ammv.models.Course;
import com.ammv.models.Unit;
import com.ammv.utils.MenuBarProvider;
import com.ammv.utils.RandomUtils;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class AddUnitToCourse implements Initializable {
    @FXML
    private BorderPane borderPane;
    @FXML
    private JFXComboBox<String> cbCourseCode;
    @FXML
    private JFXComboBox<String> cbUnitCode;
    @FXML
    private JFXButton btnSave;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        MenuBarProvider menuBarProvider = new MenuBarProvider(borderPane);
        borderPane.setTop(menuBarProvider.getMenuBar());

        btnSave.setOnAction(event -> saveUnit());

        DBConnector.DBFailureListener failureListener = reason -> RandomUtils.showAlert(Alert.AlertType.ERROR, reason);

        List<Course> courses = CourseHelper.readAll(failureListener);
        List<Unit> units = UnitHelper.readAll(failureListener);

        List<String> courseCodes = new ArrayList<>();
        List<String> unitCodes = new ArrayList<>();

        for (Course course : courses) {
            courseCodes.add(course.getCode());
        }

        for (Unit unit : units) {
            unitCodes.add(unit.getCode());
        }

        cbCourseCode.getItems().addAll(courseCodes);
        cbUnitCode.getItems().addAll(unitCodes);
    }

    private void saveUnit() {
        String courseCode = cbCourseCode.getValue();
        String unitCode = cbUnitCode.getValue();

        if (courseCode == null || unitCode == null){
            RandomUtils.showAlert(Alert.AlertType.ERROR, "All fields are required!");
            return;
        }

        CourseHelper.addUnitToCourse(unitCode, courseCode, new DBConnector.DBTask() {
            @Override
            public void taskFailed(String reason) {
                RandomUtils.showAlert(Alert.AlertType.ERROR, reason);
            }

            @Override
            public void taskSuccessful() {
                RandomUtils.showAlert(Alert.AlertType.INFORMATION, "The Unit has been added to the course.");
            }
        });
    }

}
