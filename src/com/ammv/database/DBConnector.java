package com.ammv.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Used to establish a connection to the database
 */
public class DBConnector {
    private static String DATABASE = "oop2_miniproj";
    private static String USERNAME = "root";
    private static String PASSWORD = "";

    private static Connection connection;

    /**
     * Returns an instance of a Database connection
     *
     * @return Connection
     * @throws ClassNotFoundException Thrown if it is unable to find the jdbc Driver
     * @throws SQLException Thrown if it is unable to establish a connection
     */
    static Connection getConnection() throws ClassNotFoundException, SQLException {
        if (connection == null) {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost/" + DATABASE, USERNAME, PASSWORD);
        }
        return connection;
    }

    /**
     * Used to close the connection to the database
     *
     * @throws SQLException
     */
    public static void closeConnection() throws SQLException {
        connection.close();
    }

    /**
     * Used to listen for successful and failed database operations
     */
    public interface DBTask extends DBSuccessListener, DBFailureListener {
    }

    /**
     * A listener for a successful database operation
     */
    public interface DBSuccessListener {
        /**
         * Successful database operation
         */
        void taskSuccessful();
    }

    /**
     * A listener for failed database operations
     */
    public interface DBFailureListener {
        /**
         * failed database operation
         *
         * @param reason A message of the reason the operation failed
         */
        void taskFailed(String reason);
    }
}
