package com.ammv.database;

import com.ammv.models.Room;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Used for database interaction for rooms
 */
public class RoomHelper extends BaseHelper{

    /**
     * Used to create a room in the database
     * @param room The room to be created
     * @param dbTask A listener for successful and failed database operations
     */
    public static void create(Room room, DBConnector.DBTask dbTask) {
        try {
            if (!roomExists(room.getCode())) {
                String sql = "INSERT INTO miniproj_rooms(rm_code, rm_capacity, rm_lab, build_code) VALUES (?,?,?,?)";
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, room.getCode());
                preparedStatement.setInt(2, room.getCapacity());
                preparedStatement.setInt(3, room.isLab());
                preparedStatement.setString(4, room.getBuildingCode());

                preparedStatement.execute();
                dbTask.taskSuccessful();
            } else {
                dbTask.taskFailed("A room with code " + room.getCode() + " already exists.");
            }
        } catch (SQLException sqlException) {
            dbTask.taskFailed(sqlException.getMessage());
        }

    }

    /**
     * Find one rom in the database
     * @param roomCode The code for the room to be searched for
     * @param failureListener A listener for failed database operations
     * @return The room matching the code passed in
     */
    public static Room readOne(String roomCode, DBConnector.DBFailureListener failureListener) {
        try {
            String sql = "SELECT * FROM miniproj_rooms WHERE rm_code = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, roomCode);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                String rmCode = resultSet.getString("rm_code");
                int rmCapacity = resultSet.getInt("rm_capacity");
                int rmDesign = resultSet.getInt("rm_lab");
                String buildingCode = resultSet.getString("build_code");
                return new Room(rmCode, buildingCode, rmCapacity, rmDesign);
            } else {
                return new Room("error", "error", -1, -1);
            }
        } catch (SQLException e) {
            failureListener.taskFailed(e.getMessage());
            return new Room("error", "error", -1, -1);
        }
    }

    /**
     * Find all rooms in the database
     * @param failureListener A listener for failed database operations
     * @return A list of rooms
     */
    public static List<Room> readAll(DBConnector.DBFailureListener failureListener) {
        try {
            String sql = "SELECT * FROM miniproj_rooms";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);

            return readRooms(preparedStatement);
        } catch (SQLException e) {
            failureListener.taskFailed(e.getMessage());
            return Collections.emptyList();
        }
    }

    /**
     * Find rooms which are in a given building
     * @param buildingCode The code for the building to be used a filter
     * @param failureListener A listener for failed database operations
     * @return A list of rooms
     */
    public static List<Room> readByBuilding(String buildingCode, DBConnector.DBFailureListener failureListener) {
        try {
            String sql = "SELECT * FROM miniproj_rooms WHERE build_code = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, buildingCode);

            return readRooms(preparedStatement);
        } catch (SQLException e) {
            failureListener.taskFailed(e.getMessage());
            return Collections.emptyList();
        }
    }

    /**
     * Update the value of a room
     * @param room The updated room
     * @param dbTask A listener for successful and failed database operations
     */
    public static void update(Room room, DBConnector.DBTask dbTask) {
        try {
            if (roomExists(room.getCode())) {
                String sql = "UPDATE miniproj_rooms SET rm_capacity = ?, rm_lab = ?, build_code = ? WHERE rm_code = ?";
                PreparedStatement preparedStatement = connection.prepareStatement(sql);

                preparedStatement.setInt(1, room.getCapacity());
                preparedStatement.setInt(2, room.isLab());
                preparedStatement.setString(3, room.getBuildingCode());
                preparedStatement.setString(4, room.getCode());

                preparedStatement.execute();
                dbTask.taskSuccessful();
            } else {
                dbTask.taskFailed("The room " + room.getCode() + " does not exist");
            }
        } catch (SQLException sqlException) {
            dbTask.taskFailed(sqlException.getMessage());
        }
    }

    /**
     * Delete a building
     * @param roomCode The code for the room to be deleted
     * @param dbTask A listener for successful and failed database operations
     */
    public static void delete(String roomCode, DBConnector.DBTask dbTask) {
        try {
            if (roomExists(roomCode)) {
                String sql = "DELETE FROM miniproj_rooms WHERE rm_code = ?";
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, roomCode);

                preparedStatement.execute();
                dbTask.taskSuccessful();
            } else {
                dbTask.taskFailed("Room " + roomCode + " doesn't exist");
            }
        } catch (SQLException sqlException) {
            dbTask.taskFailed(sqlException.getMessage());
        }
    }

    /**
     * Find all rooms based on a given statement
     * @param preparedStatement A statement to be executed to get a list of rooms
     * @return A list of rooms
     * @throws SQLException
     */
    private static List<Room> readRooms(PreparedStatement preparedStatement) throws SQLException {
        ResultSet resultSet = preparedStatement.executeQuery();

        List<Room> rooms = new ArrayList<>();
        while (resultSet.next()) {
            String rmCode = resultSet.getString("rm_code");
            int rmCapacity = resultSet.getInt("rm_capacity");
            int rmDesign = resultSet.getInt("rm_lab");
            String buildingCode = resultSet.getString("build_code");
            rooms.add(new Room(rmCode, buildingCode, rmCapacity, rmDesign));
        }

        return rooms;
    }

    /**
     * Used to check if a room exists
     * @param roomCode The code for the building to be searched for
     * @return boolean of whether the room exists
     * @throws SQLException
     */
    private static boolean roomExists(String roomCode) throws SQLException {
        String sql = "SELECT * FROM miniproj_rooms WHERE rm_code = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, roomCode);

        ResultSet queryResults = preparedStatement.executeQuery();

        return queryResults.next();
    }
}
