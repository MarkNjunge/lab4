package com.ammv.database;

import com.ammv.utils.RandomUtils;
import javafx.scene.control.Alert;

import java.sql.Connection;
import java.sql.SQLException;

public class BaseHelper {
    static Connection connection;

    static {
        try {
            connection = DBConnector.getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            RandomUtils.showAlert(Alert.AlertType.ERROR, e.getMessage());
            e.printStackTrace();
        }
    }
}
