package com.ammv.utils;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

public class RandomUtils {
    public static void showAlert(Alert.AlertType alertType, String message) {
        Alert alert = new Alert(alertType, message, ButtonType.OK);
        alert.setHeaderText(null);
        alert.setTitle(alertType.toString());
        alert.show();
    }
}
