package com.ammv.utils;

import com.ammv.database.DBConnector;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Used to create a menu bar to be used in all files
 */
public class MenuBarProvider {
    private MenuBar menuBar;
    private Node node;

    /**
     * @param node A node to be used get the current stage
     */
    public MenuBarProvider(Node node) {
        this.menuBar = new MenuBar();
        this.node = node;

        addFileMenu();

        addMenu("Building", "AddBuilding.fxml", "ViewBuildings.fxml", "EditBuilding.fxml");
        addMenu("Room", "AddRoom.fxml", "ViewRooms.fxml", "EditRoom.fxml");
        addMenu("Course", "AddCourse.fxml", "ViewCourses.fxml", "EditCourse.fxml");
        addMenu("Unit", "AddUnit.fxml", "ViewUnits.fxml", "EditUnit.fxml");
    }

    /**
     * Used to get an instance of the menu bar
     *
     * @return MenuBar
     */
    public MenuBar getMenuBar() {
        return menuBar;
    }

    /**
     * Add a menu items under <i>file</i>
     */
    private void addFileMenu() {
        Menu menu = new Menu("File");

        MenuItem about = new MenuItem("About");
        MenuItem close = new MenuItem("Close");

        about.setOnAction(event -> goToFile("About.fxml"));
        close.setOnAction(event -> {
            try {
                DBConnector.closeConnection();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            Stage stage = (Stage) node.getScene().getWindow();
            stage.close();
        });

        menu.getItems().add(about);
        menu.getItems().add(close);

        menuBar.getMenus().add(menu);
    }

    /**
     * Add menu items for ...
     * @param menuTitle Title for the menu
     * @param addUI The filename for <i>File</i>
     * @param viewUI The filename for <i>View</i>
     * @param editUI The filename for <i>Edit</i>
     */
    private void addMenu(String menuTitle, String addUI, String viewUI, String editUI) {
        Menu menu = new Menu(menuTitle);

        MenuItem add = new MenuItem("Add");
        MenuItem view = new MenuItem("View");
        MenuItem edit = new MenuItem("Edit");

        add.setOnAction(event -> goToFile(addUI));
        view.setOnAction(event -> goToFile(viewUI));
        edit.setOnAction(event -> goToFile(editUI));

        if (menuTitle.equals("Course")) {
            MenuItem addUnitToCourse = new MenuItem("Add Unit to course");
            addUnitToCourse.setOnAction(event -> goToFile("AddUnitToCourse.fxml"));
            menu.getItems().add(addUnitToCourse);
        }

        menu.getItems().add(add);
        menu.getItems().add(view);
        menu.getItems().add(edit);

        menuBar.getMenus().add(menu);
    }

    /**
     * Opens a file on the current stage
     * @param filename The name of the file to be opened
     */
    private void goToFile(String filename) {
        try {
            Parent root = FXMLLoader.load(getClass().getClassLoader().getResource(filename));
            Stage stage = (Stage) node.getScene().getWindow();
            String title = filename.split(".fxml")[0];
            stage.setTitle(title);
            stage.setScene(new Scene(root, 800, 600));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
