package com.ammv.models;

public class CourseUnit {
    private String courseCode;
    private String unitCode;

    public CourseUnit(String courseCode, String unitCode) {
        this.courseCode = courseCode;
        this.unitCode = unitCode;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public String getUnitCode() {
        return unitCode;
    }
}
