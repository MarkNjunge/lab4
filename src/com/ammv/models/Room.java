package com.ammv.models;

public class Room {
    private String code;
    private String buildingCode;
    private int capacity;
    private int lab;

    public Room(String code, String buildingCode, int capacity, int lab) {
        this.code = code;
        this.buildingCode = buildingCode;
        this.capacity = capacity;
        this.lab = lab;
    }

    public String getCode() {
        return code;
    }

    public String getBuildingCode() {
        return buildingCode;
    }

    public int getCapacity() {
        return capacity;
    }

    public int isLab() {
        return lab;
    }
}
