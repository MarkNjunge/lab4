package com.ammv.models;

import java.util.List;

public class Building {
    private String code;
    private String name;
    private List<Room> rooms;

    public Building(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public List<Room> getRooms() {
        return rooms;
    }
}
